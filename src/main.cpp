#include <Arduino.h>
#include <Keypad.h>
#include <LiquidCrystal_I2C.h>

const byte ROWS = 4; // four rows
const byte COLS = 3; // four columns
// define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
    {'1', '2', '3'},
    {'4', '5', '6'},
    {'7', '8', '9'},
    {'*', '0', '#'}};

// important for "Storm kaypad PLX12T203 (3 x 4) Matrix"
byte rowPins[ROWS] = {9, 8, 2, 3}; // connect to the row pinouts of the keypad
byte colPins[COLS] = {7, 6, 5};    // connect to the column pinouts of the keypad

static char buildStr[16];
static byte kpadState;
static byte buildCount;
static int passCount = 0;
static uint lcdCols = 16;
static uint lcdRows = 2;
// static byte pressCount;

void numpadEvent(KeypadEvent key);
void swOnState(char key);
void resetPasswordInputAndDisplay();
bool validatePassword();
bool callS7OpenDoor();

Keypad numpad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); // initialize an instance of class NewKeypad
LiquidCrystal_I2C lcd(0x27, lcdCols, lcdRows);                              // I2C address 0x27, 16 column and 2 rows

void setup()
{
    Serial.begin(9600);
    lcd.init(); // initialize the lcd
    lcd.backlight();

    numpad.begin(makeKeymap(hexaKeys));
    numpad.addEventListener(numpadEvent); // Add an event listener.
    numpad.setHoldTime(2000);             // Default is 1000mS
}

void loop()
{
    numpad.getKey();
}

void numpadEvent(KeypadEvent key)
{
    // in here when using number keypad
    kpadState = numpad.getState();
    swOnState(key);
} // end numbers keypad events

void swOnState(char key)
{
    switch (kpadState)
    {
    case PRESSED:
        if ((key != '#') && (key != '*'))
        { // start with passwort input
            if (passCount == 0)
            {
                lcd.clear();
                lcd.setCursor(3, 0);   // move cursor to   (3, 0)
                lcd.print("Passcode"); // print message at (3, 0)
                lcd.setCursor(0, 1);   // move cursor to   (0, 1)
            }
            lcd.blink();
            lcd.write('*');
            passCount++;
        }
        else if (key == '#')
        { // TODO fill
            if (passCount == 0)
                break;
            bool isValid = validatePassword();
            if (isValid)
            {
                if (callS7OpenDoor())
                {
                    resetPasswordInputAndDisplay();
                    lcd.setCursor(4, 0);   // move cursor to   (0, 0)
                    lcd.print("Welcome!"); // print message at (0, 0)
                }
            }
        }
        else
        { // abort password input
            resetPasswordInputAndDisplay();
        }
        break;
    case HOLD:
        if ((key == '#') || (key == '*'))
        {
            resetPasswordInputAndDisplay();
        }
        if (key == '#')
        {
            lcd.setCursor(3, 0);    // move cursor to   (3, 0)
            lcd.print("Zieschang"); // print message at (3, 0)
        }
        else if (key == '*')
        {
            
        }
        break;

    case RELEASED:
        if (buildCount >= sizeof(buildStr))
            buildCount = 0; // Our string is full. Start fresh.
        break;
    } // end switch-case
} // end switch on state function

void resetPasswordInputAndDisplay()
{
    passCount = 0;
    lcd.clear(); // clear display
    lcd.noBlink();
}

bool validatePassword()
{
    // TODO fill with logic
    return true;
}

bool callS7OpenDoor()
{
    // TODO fill with logic
    return true;
}
